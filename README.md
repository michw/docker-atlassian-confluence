![Atlassian Confluence Server](https://wac-cdn.atlassian.com/dam/jcr:a22c9f02-b225-4e34-9f1d-e5ac0265e543/confluence_rgb_blue.png?cdnVersion=k =250x)

Confluence is an on-premises wiki software. Create spaces, pages, attach files nad such.

Learn more about Confluence: <https://www.atlassian.com/software/confluence>

# Overview

This Docker container makes it easy to get an instance of Confluence up and running
for evaluative and production purposes.

# Quick Start

For the `CONFLUENCE_HOME` directory that is used to store the application data, attachments and such, 
we recommend mounting a host directory as a [data volume](https://docs.docker.com/userguide/dockervolumes/#mount-a-host-directory-as-a-data-volume):

Set permissions for the data directory so that the runuser can write to it:

    $> docker run -u root -v /data/confluence:/var/atlassian/application-data/confluence docker.local/confluence chown -R daemon  /var/atlassian/application-data/confluence

Start Atlassian Confluence Server:

    $> docker run -v /data/confluence:/var/atlassian/application-data/confluence --name="confluence" -d -p 8090:8090 docker.local/confluence

**Success**. Confluence is now available on [http://localhost:8090](http://localhost:8090)*

Please ensure your container has the necessary resources allocated to it.
We recommend 2GiB of memory allocated to accommodate the application server
See [Supported Platforms](https://confluence.atlassian.com/doc/supported-platforms-207488198.html) for further information.
    

_* Note: If you are using `docker-machine` on Mac OS X, please use `open http://$(docker-machine ip default):8090` instead._

# Upgrade

To upgrade to a more recent version of Confluence you can simply stop the `confluence`
container and start a new one based on a more recent image:

    $> docker stop confluence
    $> docker rm confluence
    $> docker run ... (See above)

As your data is stored in the data volume directory on the host it will still
be available after the upgrade.

_Note: Please make sure that you **don't** accidentally remove the `confluence`
container and its volumes using the `-v` option._

# Running behind apache/nginx proxy

First, take a look at Atlassian documentation about How to run Confluence behind proxy: <https://confluence.atlassian.com/conf58/using-apache-with-mod_proxy-771892874.html>
Surprisingly, `server.xml` is part of application (docker image) itself. It is not stored home directory you can mount as volume. Atlassian, that is a very bad habbit.


For now, just add another command to Dockerfile:
```Dockerfile
# set rev-proxy here
RUN sed -i -e"s/Connector port=\"8090\" connectionTimeout=\"20000\" redirectPort=\"8443\"/Connector port=\"8090\" connectionTimeout=\"20000\" redirectPort=\"8443\"\nproxyName=\"proxy.host.name.net\" proxyPort=\"443\" scheme=\"https\"/g" $CONFLUENCE_INSTALL_DIR/conf/server.xml

```


# Backup

Backup mechanism is built into Confluence. For small and medium sites, just click on **cog icon**, then choose **General Configuration** and click **Backup Administration** in the **Configuration** section. More information can be found on [Atlassian site](https://confluence.atlassian.com/doc/configuring-backups-138348.html)


# Issue tracker

All comments are more than welcome! Please raise an
[issue](https://gitlab.com/michw/docker-atlassian-confluence/issues) if you
encounter any problems with this Dockerfile.

